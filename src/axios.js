import axios from 'axios';

const instance = axios.create({
  // baseURL: 'http://localhost:4000/',
  // baseURL: 'https://burger-builder-9ff36.firebaseio.com/',
});

export default instance;
