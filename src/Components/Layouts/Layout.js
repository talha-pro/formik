import React from 'react';
import Auxilliary from '../../hoc/Auxilliary';
import FormData from '../Form/FormData/FormData';

const Layout = () => {
  return (
    <Auxilliary>
      <FormData />
    </Auxilliary>
  );
};

export default Layout;
