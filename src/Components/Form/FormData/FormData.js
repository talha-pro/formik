import React, { Component } from 'react';
import FormikForm from '../CustomForm';

class FormData extends Component {
  state = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    address: '',
  };

  render() {
    return (
      <FormikForm
        firstname={this.state.firstname}
        lastname={this.state.lastname}
        email={this.state.email}
        password={this.state.password}
        address={this.state.address}
      />
    );
  }
}

export default FormData;
