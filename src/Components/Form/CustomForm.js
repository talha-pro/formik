import React from 'react';
import classes from './CustomForm.module.css';
import Auxilliary from '../../hoc/Auxilliary';
import { withFormik, Form, Field } from 'formik';
import axios from '../../axios';

const customForm = () => {
  return (
    <Auxilliary>
      <Form className={classes.CustomForm}>
        <h3>TAYBRA</h3>
        <Field
          className={classes.Label}
          type='text'
          name='firstname'
          placeholder='First Name'
        />
        <Field
          className={classes.Label}
          type='text'
          name='lastname'
          placeholder='Last Name'
        />
        <Field
          className={classes.Label}
          type='text'
          name='email'
          placeholder='Email'
        />
        <Field
          className={classes.Label}
          type='password'
          name='password'
          placeholder='Password'
        />
        <Field
          className={classes.Label}
          type='text'
          name='address'
          placeholder='Address'
        />
        <br />
        <button type='submit' className={classes.Button}>
          Submit
        </button>
      </Form>
    </Auxilliary>
  );
};

const FormikForm = withFormik({
  mapPropsToValues({ firstname, lastname, email, password, address }) {
    return {
      firstname: firstname || '',
      lastname: lastname || '',
      email: email || '',
      password: password || '',
      address: address || '',
    };
  },
  handleSubmit(values) {
    // console.log(values);
    axios
      .post('http://localhost:4000/', values)
      // .get('http://localhost:4000/')
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  },
})(customForm);

export default FormikForm;
