const express = require('express');

const mongoose = require('mongoose');

const User = require('./mongodb/models/user');

const bodyParser = require('body-parser');

const app = express();

mongoose.connect('mongodb://localhost/users');
mongoose.Promise = global.Promise;

const cors = require('cors');

app.use(cors());

app.use(bodyParser.json());

// app.get('/', (req, res) => {
//   res.send({ message: 'GET Request Completed' });
//   console.log('GET Response Sent');
// });

app.post('/', (req, res) => {
  User.create(req.body).then(function (user) {
    res.send(user);
  });
});

app.listen(4000, () => {
  console.log(`Express Server is now running`);
});
