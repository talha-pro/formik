import React from 'react';
import Layout from './Components/Layouts/Layout';
import classes from './App.module.css';

const App = () => {
  return (
    <div className={classes.App}>
      <Layout />
    </div>
  );
};

export default App;
